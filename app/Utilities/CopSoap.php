<?php


namespace App\Utilities;


use Illuminate\Support\Facades\Log;
use SoapClient;
use SoapVar;

class CopSoap
{
  public static function createAnagrafica($xml)
  {

    if (env('APP_ENV', 'test') == 'production') {
      // Prod
      $endpoint       = env('ENDPOINT_PROD', '');
      $wsdl           = $endpoint . '?wsdl';
      $certificate    = base_path() . '/certs/' . env('CERT_PROD', '');
      $password       = env('CERT_PROD_PASSWORD', '');
    } else {
      // test
      $endpoint       = env('ENDPOINT_TEST', '');
      $wsdl           = $endpoint . '?wsdl';
      $certificate    = base_path() . '/certs/' . env('CERT_TEST', '');
      $password       = env('CERT_TEST_PASSWORD', '');
    }

    $options = array(
      'location'      => $endpoint,
      'keep_alive'    => true,
      'trace'         => true,
      'local_cert'    => $certificate,
      'passphrase'    => $password,
      'cache_wsdl'    => WSDL_CACHE_NONE
    );

    $soapClient = new SoapClient($wsdl, $options);

    $request = new SoapVar($xml, XSD_ANYXML);
    $result = $soapClient->CreateAnagrafica($request);

    if (!property_exists($result, 'CodiceFornitore')) {

      $errors = array();
      if (property_exists($result, 'LogControlli')) {
        $errors['log_controlli']=$result->LogControlli;
      }
      if (property_exists($result, 'LogTransazione')) {
        $errors['log_transazione']=$result->LogTransazione;
      }
      Log::error($xml, $errors);
      throw new \Exception(json_encode($errors));
    }

    return $result->CodiceFornitore;

  }

  /**
   * @param $xml
   * @return mixed
   * @throws \Exception
   */
  public static function createImpegno($xml)
  {

    if (env('APP_ENV', 'test') == 'production') {
      // Prod
      $endpoint       = env('ENDPOINT_PROD', '');
      $wsdl           = $endpoint . '?wsdl';
      $certificate    = base_path() . '/certs/' . env('CERT_PROD', '');
      $password       = env('CERT_PROD_PASSWORD', '');
    } else {
      // test
      $endpoint       = env('ENDPOINT_TEST', '');
      $wsdl           = $endpoint . '?wsdl';
      $certificate    = base_path() . '/certs/' . env('CERT_TEST', '');
      $password       = env('CERT_TEST_PASSWORD', '');
    }

    $options = array(
      'location'      => $endpoint,
      'keep_alive'    => true,
      'trace'         => true,
      'local_cert'    => $certificate,
      'passphrase'    => $password,
      'cache_wsdl'    => WSDL_CACHE_NONE
    );

    $soapClient = new SoapClient($wsdl, $options);
    $request = new SoapVar($xml, XSD_ANYXML);
    $result = $soapClient->CreateFounds($request);
    //$result = $soapClient->__soapCall($function, $request);


    if (!property_exists($result, 'Relazioni')) {

      //!property_exists($result->Relazioni, 'FondiAccantonati') &&
      //!property_exists($result->Relazioni->FondiAccantonati, 'NumeroDocumento'
      $errors = array();
      if (property_exists($result, 'LogControlli')) {
        foreach ($result->LogControlli as $l) {
          if ($l->TipoMessaggio == 'W') {
            $errors['log_controlli']['warning'][]= $l->TestoMessaggio;
          } else {
            $errors['log_controlli']['error'][]= $l->TestoMessaggio;
          }
        }
        //$errors['log_controlli']=$result->LogControlli;
      }
      if (property_exists($result, 'LogTransazione')) {
        $errors['log_transazione']=$result->LogTransazione;
      }
      Log::error($xml, $errors);
      // Todo: create custom exception
      throw new \Exception(json_encode($errors));
    }

    $foundsData = [];
    if (is_array($result->Relazioni)) {
      foreach ($result->Relazioni as $r) {
        var_dump($r);
        $applicationsId = $r->IdentificativoApplicazioneSettore->CodicePratica;
        $foundsData[$applicationsId] = [
          'numero_documento' => $r->FondiAccantonati->NumeroDocumento,
          'posizione' => $r->FondiAccantonati->Posizione
        ];
      }
    } else {
      $applicationsId = $result->Relazioni->IdentificativoApplicazioneSettore->CodicePratica;
      $foundsData[$applicationsId] = [
        'numero_documento' => $result->Relazioni->FondiAccantonati->NumeroDocumento,
        'posizione' => $result->Relazioni->FondiAccantonati->Posizione
      ];
    }


    return $foundsData;

  }
}
