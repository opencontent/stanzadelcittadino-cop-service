<?php


namespace App\Utilities;


use App\Models\Application;
use Illuminate\Support\Facades\App;

class Db
{

  public static function getApplication($remoteId)
  {
    $result = app('db')->select("SELECT * FROM applications WHERE remote_id = '". $remoteId ."'");
    if (!empty($result)) {
      return $result[0];
    }
    return null;
  }

  public static function insertApplication($remoteId)
  {
    if (app('db')->insert("INSERT INTO applications (remote_id, status, logs) VALUES  (?, '', '')", [$remoteId])) {
      return self::getApplication($remoteId);
    }
    return false;
  }

  public static function error($id, $message = '')
  {
    //$update = app('db')->table('applications')->where('id', $id)->update(['name' => $name, 'lasname' => $lastname]);

    app('db')->update(
      "UPDATE applications SET status = 'error', logs = ? WHERE remote_id = ?",
      [
        $message,
        $id
      ]
    );
  }

  public static function success($id, $message = '')
  {
    app('db')->insert(
      "UPDATE applications SET status = 'success', logs = ? WHERE remote_id = ?",
      [
        $message,
        $id
      ]
    );
  }

  public static function saveApplication(Application $application, $attributes)
  {
    if (!empty($attributes)) {
      foreach ($attributes as $k => $v) {
        $application->setAttribute($k, $v);
        $application->save();
      }
    }
  }
}
