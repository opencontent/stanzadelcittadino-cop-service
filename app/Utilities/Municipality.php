<?php


namespace App\Utilities;


use Illuminate\Support\Facades\Http;

class Municipality
{
  const JSON_RESOURCE_URL = 'https://json-server.opencontent.it/comuni_istat';

  /**
   * @param $name
   * @return mixed
   * @throws \Exception
   */
  public static function getData($name)
  {
    if (empty($name)) {
      throw new \Exception('Empty municipality name');
    }

    $searchUrl = self::JSON_RESOURCE_URL . '?comune_lc=' . urlencode(trim(strtolower($name)));
    //$data = json_decode(file_get_contents( $searchUrl ), true);
    $response = Http::get($searchUrl);
    $data = $response->json();

    if (empty($data)) {
      throw new \Exception('Municipality ' . $name . ' not found --> ' . $searchUrl);
    }

    return $data[0];
  }
}
