<?php


namespace App\Mapper;


use App\Objects\Anagrafica;
use App\Objects\Impegno;
use App\Objects\ImpegnoMulti;
use App\Objects\PosizioneImpegno;
use App\Utilities\Municipality;
use Ramsey\Uuid\Uuid;

class BonusVacanza
{

  /**
   * @param $row
   * @return Anagrafica
   * @throws \Exception
   */
  public static function mapAnagrafica($row)
  {
    $anagrafica = new Anagrafica();
    $anagrafica->setNome($row['data.applicant.data.completename.data.name']); // Name
    $anagrafica->setCognome($row['data.applicant.data.completename.data.surname']); // Lastname
    $anagrafica->setCodiceFiscale($row['data.applicant.data.fiscal_code.data.fiscal_code']); // FiscalCode
    $anagrafica->setSesso($row['data.applicant.data.gender.data.gender']); // Gender
    $anagrafica->setVia($row['data.applicant.data.address.data.address']); // Address
    $anagrafica->setLocalita($row['data.applicant.data.address.data.municipality']); // Municipality
    $anagrafica->setCap($row['data.applicant.data.address.data.postal_code']); //
    $anagrafica->setProvinciaResidenza($row['data.applicant.data.address.data.county']);

    $municipality = Municipality::getData($row['data.applicant.data.address.data.municipality']);

    $anagrafica->setCodiceProvincia(trim($municipality['codice_provincia']));
    $anagrafica->setCodiceComune(trim($municipality['codice_comune']));
    $anagrafica->setCodiceRegione(trim($municipality['codice_regione']));
    $anagrafica->setTelefono($row['data.cell_number']);

    $anagrafica->setDataNascita($row['data.applicant.data.Born.data.natoAIl']);
    $anagrafica->setIban($row['data.iban']);

    return $anagrafica;
  }

  /**
   * @param $row
   * @return Impegno
   */
  public static function mapImpegno($row)
  {

    $codice = $row['codice_pratica'];

    $impegno = new Impegno();
    $impegno->setData(date('Ymd'));
    $impegno->setIdentificativoLista($codice);
    $impegno->setTesto('LP 6/2020 ART. 42 - BONUS VACANZA');
    $impegno->setImporto(trim(str_replace(['€', ','], ['', '.'], $row['data.bonus_amount'])));
    $impegno->setCodiceFornitore($row['codice_fornitore']);

    if (!empty($row['POSIZIONE_CC'])) {
      $impegno->setTipoBancaPartner($row['POSIZIONE_CC']);
    }

    $impegno->setProvvedimento($row['PROVVEDIMENTO']);
    $impegno->setTipoProvvedimento('DTD');
    $impegno->setCentroCosto('D336');
    $impegno->setTestoPosizione($row['id']);
    $impegno->setCodicePratica($codice);
    $impegno->setChiaveRata('1');
    $impegno->setPosizioneDocRiferimento('001');

    if (env('APP_ENV', 'test') == 'production') {
      $impegno->setCapitolo('615644');
      $impegno->setNumeroDocumento('2016769');
    } else {
      $impegno->setCapitolo('615510');
      $impegno->setNumeroDocumento('2016740');
    }


    return $impegno;
  }

  /**
   * @param $row
   * @return ImpegnoMulti
   */
  public static function mapImpegnoMulti($row)
  {
    $impegno = new ImpegnoMulti();
    $impegno->setData(date('Ymd'));
    $impegno->setIdentificativoLista(time());
    $impegno->setTesto('LP 6/2020 ART. 42 - BONUS VACANZA');
    $impegno->setProvvedimento($row['PROVVEDIMENTO']);
    $impegno->setTipoProvvedimento('DTD');

    return $impegno;
  }

  /**
   * @param $row
   * @return PosizioneImpegno
   */
  public static function mapPosizioneImpegno($row)
  {
    $posizioneimpegno = new PosizioneImpegno();
    $posizioneimpegno->setImporto($row['data.bonus_amount']);
    $posizioneimpegno->setCodiceFornitore($row['codice_fornitore']);

    if (!empty($row['POSIZIONE_CC'])) {
      $posizioneimpegno->setTipoBancaPartner($row['POSIZIONE_CC']);
    } else {
      $posizioneimpegno->setTipoBancaPartner(1);
    }

    $posizioneimpegno->setCentroCosto('D336');
    $posizioneimpegno->setTesto($row['id']);

    if (env('APP_ENV', 'test') == 'production') {
      $posizioneimpegno->setCapitolo('615644');
      $posizioneimpegno->setNumeroDocumento('2016769');
    } else {
      $posizioneimpegno->setCapitolo('615510');
      $posizioneimpegno->setNumeroDocumento('2016740');
    }

    $posizioneimpegno->setCodicePratica($row['codice_pratica']);
    $posizioneimpegno->setChiaveRata('1');
    $posizioneimpegno->setdocRiferimento('001');

    $posizioneimpegno->setLocalitaFornitore($row['data.applicant.data.address.data.municipality']);

    return $posizioneimpegno;
  }

}
