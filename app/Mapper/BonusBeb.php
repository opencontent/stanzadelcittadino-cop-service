<?php


namespace App\Mapper;


use App\Objects\Anagrafica;
use App\Objects\Impegno;
use App\Objects\ImpegnoMulti;
use App\Objects\PosizioneImpegno;
use App\Utilities\Municipality;
use Ramsey\Uuid\Uuid;

class BonusBeb
{

  /**
   * @param $row
   * @return Anagrafica
   * @throws \Exception
   */
  public static function mapAnagrafica($row)
  {
    $anagrafica = new Anagrafica();
    $anagrafica->setNome($row['Richiedente/Nome']); // Name
    $anagrafica->setCognome($row['Richiedente/Cognome']); // Lastname
    $anagrafica->setCodiceFiscale($row['Codice fiscale']); // FiscalCode
    $anagrafica->setSesso($row['Richiedente/Genere']); // Gender
    $anagrafica->setVia($row['Richiedente/Indirizzo'] . ' ' . $row['Richiedente/Numero Civico']); // Address
    $anagrafica->setLocalita($row['Richiedente/Comune']); // Municipality
    $anagrafica->setCap($row['Richiedente/CAP']); //
    $anagrafica->setProvinciaResidenza($row['Richiedente/Provincia']);

    $municipality = Municipality::getData($row['Richiedente/Comune']);

    $anagrafica->setCodiceProvincia(trim($municipality['codice_provincia']));
    $anagrafica->setCodiceComune(trim($municipality['codice_comune']));
    $anagrafica->setCodiceRegione(trim($municipality['codice_regione']));
    $anagrafica->setTelefono('');

    $anagrafica->setDataNascitaFromFormat($row['Richiedente/Data di nascita']);
    $anagrafica->setIban($row['Dichiarazioni/IBAN']);

    return $anagrafica;
  }

  /**
   * @param $row
   * @return ImpegnoMulti
   */
  public static function mapImpegnoMulti($row)
  {
    $impegno = new ImpegnoMulti();
    $impegno->setData(date('Ymd'));
    $impegno->setIdentificativoLista(time());
    $impegno->setTesto('L.p. 3/2020 art. 5, comma 6 Bonus BeB');
    $impegno->setProvvedimento($row['PROVVEDIMENTO']);
    $impegno->setTipoProvvedimento('DTD');

    return $impegno;
  }

  /**
   * @param $row
   * @return PosizioneImpegno
   */
  public static function mapPosizioneImpegno($row)
  {
    $posizioneimpegno = new PosizioneImpegno();
    $posizioneimpegno->setImporto($row['Richiedente/Importo del bonus']);
    $posizioneimpegno->setCodiceFornitore($row['codice_fornitore']);

    if (!empty($row['POSIZIONE_CC'])) {
      $posizioneimpegno->setTipoBancaPartner($row['POSIZIONE_CC']);
    } else {
      $posizioneimpegno->setTipoBancaPartner(1);
    }

    $posizioneimpegno->setCentroCosto('S039');
    $posizioneimpegno->setTesto($row['id']);

    if (env('APP_ENV', 'test') == 'production') {
      // Parametri per la produzione
      $posizioneimpegno->setCapitolo('615547');
      $posizioneimpegno->setNumeroDocumento('2016837');
    } else {
      $posizioneimpegno->setCapitolo('615510');
      $posizioneimpegno->setNumeroDocumento('2016740');
    }

    $posizioneimpegno->setCodicePratica($row['codice_pratica']);
    $posizioneimpegno->setChiaveRata('1');
    $posizioneimpegno->setdocRiferimento('001');

    if (isset($row['FILI']) && !empty(trim($row['FILI']))) {
      $posizioneimpegno->setLocalitaFornitore( $row['Richiedente/Comune di']);
    } else {
      $posizioneimpegno->setLocalitaFornitore( $row['Richiedente/Comune']);
    }


    return $posizioneimpegno;
  }

}
