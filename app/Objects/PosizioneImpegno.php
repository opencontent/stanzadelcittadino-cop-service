<?php


namespace App\Objects;


use SimpleXMLElement;

class PosizioneImpegno
{

  private $progressivo = 1;

  private $flagControlloFornitore = 'N';

  private $importo;

  private $codiceFornitore;

  private $tipoBancaPartner = 1;

  private $testo;

  private $capitolo;

  private $dataScadenza;

  private $centroCosto;

  private $modalitaDiPagamento = 'B';

  private $numeroDocumento;

  private $docRiferimento;

  private $codicePratica;

  private $chiaveRata;

  private $localitaFornitore;

  /**
   * PosizioneImpegno constructor.
   * @param $dataScadenza
   */
  public function __construct()
  {
    $this->dataScadenza = date('Y' . '1231');
  }

  /**
   * @return mixed
   */
  public function getProgressivo()
  {
    return $this->progressivo;
  }

  /**
   * @param mixed $progressivo
   */
  public function setProgressivo($progressivo): void
  {
    $this->progressivo = $progressivo;
  }

  /**
   * @return string
   */
  public function getFlagControlloFornitore(): string
  {
    return $this->flagControlloFornitore;
  }

  /**
   * @param string $flagControlloFornitore
   */
  public function setFlagControlloFornitore(string $flagControlloFornitore): void
  {
    $this->flagControlloFornitore = $flagControlloFornitore;
  }

  /**
   * @return mixed
   */
  public function getImporto()
  {
    return $this->importo;
  }

  /**
   * @param mixed $importo
   */
  public function setImporto($importo): void
  {
    $importo = trim(str_replace(['€', '.'], ['', ''], $importo));
    $importo = str_replace([','], ['.'], $importo);
    $this->importo = $importo;
  }

  /**
   * @return mixed
   */
  public function getCodiceFornitore()
  {
    return $this->codiceFornitore;
  }

  /**
   * @param mixed $codiceFornitore
   */
  public function setCodiceFornitore($codiceFornitore): void
  {
    $this->codiceFornitore = str_pad($codiceFornitore, 10, "0", STR_PAD_LEFT);
  }

  /**
   * @return int
   */
  public function getTipoBancaPartner(): int
  {
    return $this->tipoBancaPartner;
  }

  /**
   * @param int $tipoBancaPartner
   */
  public function setTipoBancaPartner(int $tipoBancaPartner): void
  {
    $this->tipoBancaPartner = $tipoBancaPartner;
  }

  /**
   * @return mixed
   */
  public function getTesto()
  {
    return $this->testo;
  }

  /**
   * @param mixed $testo
   */
  public function setTesto($testo): void
  {
    $this->testo = $testo;
  }

  /**
   * @return mixed
   */
  public function getCapitolo()
  {
    return $this->capitolo;
  }

  /**
   * @param mixed $capitolo
   */
  public function setCapitolo($capitolo): void
  {
    $this->capitolo = $capitolo;
  }

  /**
   * @return false|string
   */
  public function getDataScadenza()
  {
    return $this->dataScadenza;
  }

  /**
   * @param false|string $dataScadenza
   */
  public function setDataScadenza($dataScadenza): void
  {
    $this->dataScadenza = $dataScadenza;
  }

  /**
   * @return mixed
   */
  public function getCentroCosto()
  {
    return $this->centroCosto;
  }

  /**
   * @param mixed $centroCosto
   */
  public function setCentroCosto($centroCosto): void
  {
    $this->centroCosto = $centroCosto;
  }

  /**
   * @return mixed
   */
  public function getModalitaDiPagamento()
  {
    return $this->modalitaDiPagamento;
  }

  /**
   * @param mixed $modalitaDiPagamento
   */
  public function setModalitaDiPagamento($modalitaDiPagamento): void
  {
    $this->modalitaDiPagamento = $modalitaDiPagamento;
  }

  /**
   * @return mixed
   */
  public function getNumeroDocumento()
  {
    return $this->numeroDocumento;
  }

  /**
   * @param mixed $numeroDocumento
   */
  public function setNumeroDocumento($numeroDocumento): void
  {
    $this->numeroDocumento = $numeroDocumento;
  }

  /**
   * @return mixed
   */
  public function getDocRiferimento()
  {
    return $this->docRiferimento;
  }

  /**
   * @param mixed $docRiferimento
   */
  public function setDocRiferimento($docRiferimento): void
  {
    $this->docRiferimento = $docRiferimento;
  }

  /**
   * @return mixed
   */
  public function getCodicePratica()
  {
    return $this->codicePratica;
  }

  /**
   * @param mixed $codicePratica
   */
  public function setCodicePratica($codicePratica): void
  {
    $this->codicePratica = $codicePratica;
  }

  /**
   * @return mixed
   */
  public function getChiaveRata()
  {
    return $this->chiaveRata;
  }

  /**
   * @param mixed $chiaveRata
   */
  public function setChiaveRata($chiaveRata): void
  {
    $this->chiaveRata = $chiaveRata;
  }

  /**
   * @return mixed
   */
  public function getLocalitaFornitore()
  {
    return $this->localitaFornitore;
  }

  /**
   * @param mixed $localitaFornitore
   */
  public function setLocalitaFornitore($localitaFornitore): void
  {
    $this->localitaFornitore = $localitaFornitore;
  }

  public function toXml()
  {

    $xml = new SimpleXMLElement('<m:CreazioneImpegniPosizione />');

    // CreazioneImpegniPosizione

    $xml->addChild('m:CodiceTransazione', $this->codiceTransazione);
    $xml->addChild('m:Progressivo', $this->progressivo);
    $xml->addChild('m:FlagControlloFornitore', $this->flagControlloFornitore);
    $xml->addChild('m:Importo', $this->importo);

    // DatiFornitore
    $datiFornitore = $xml->addChild('m:DatiFornitore');
    $datiFornitore->addChild('m:CodiceFornitore', $this->codiceFornitore);
    $datiFornitore->addChild('m:TipoBancaPartner', $this->tipoBancaPartner);

    // DatiPosizione
    $datiPosizione = $xml->addChild('m:DatiPosizione');
    $datiPosizione->addChild('m:Testo', $this->testo);
    $datiPosizione->addChild('m:Capitolo', $this->capitolo);
    $datiPosizione->addChild('m:DataScadenza', $this->dataScadenza); // 20201231
    $datiPosizione->addChild('m:CentroCosto', $this->centroCosto); // D336
    $datiPosizione->addChild('m:ModalitaDiPagamento', $this->modalitaDiPagamento); // B
    //$datiPosizione->addChild('m:CodicePrelFondoUE', '2016740');
    //$datiPosizione->addChild('m:CodiceVLivello', '1');

    $docRiferimento = $xml->addChild('m:DocRiferimento');
    $docRiferimento->addChild('m:NumeroDocumento', $this->numeroDocumento);
    $docRiferimento->addChild('m:Posizione', $this->docRiferimento);

    // IdentificativoApplicazioneSettore
    $identificativoApplicazioneSettore = $xml->addChild('m:IdentificativoApplicazioneSettore');
    $identificativoApplicazioneSettore->addChild('m:CodicePratica', $this->codicePratica);
    $identificativoApplicazioneSettore->addChild('m:ChiaveRata', $this->chiaveRata);
  }
}
