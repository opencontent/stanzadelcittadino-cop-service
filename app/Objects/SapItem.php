<?php


namespace App\Objects;


abstract class SapItem
{
    protected $codiceApplicazione = 'SDC';

    protected $societa = 'PAT0';

    protected $codiceFornitoreApplicazione = 'WEB-SDC';

    protected $utente = 'SDC';

    /**
     * @return string
     */
    public function getCodiceApplicazione(): string
    {
        return $this->codiceApplicazione;
    }

    /**
     * @param string $codiceApplicazione
     */
    public function setCodiceApplicazione(string $codiceApplicazione): void
    {
        $this->codiceApplicazione = $codiceApplicazione;
    }

    /**
     * @return string
     */
    public function getSocieta(): string
    {
        return $this->societa;
    }

    /**
     * @param string $societa
     */
    public function setSocieta(string $societa): void
    {
        $this->societa = $societa;
    }

    /**
     * @return string
     */
    public function getCodiceFornitoreApplicazione(): string
    {
        return $this->codiceFornitoreApplicazione;
    }

    /**
     * @param string $codiceFornitoreApplicazione
     */
    public function setCodiceFornitoreApplicazione(string $codiceFornitoreApplicazione): void
    {
        $this->codiceFornitoreApplicazione = $codiceFornitoreApplicazione;
    }

    /**
     * @return string
     */
    public function getUtente(): string
    {
        return $this->utente;
    }

    /**
     * @param string $utente
     */
    public function setUtente(string $utente): void
    {
        $this->utente = $utente;
    }

}
