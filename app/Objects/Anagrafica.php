<?php

namespace App\Objects;

use App\Objects\SapItem;
use DateTime;
use SimpleXMLElement;

class Anagrafica extends SapItem
{

  const GENDER = [
    'maschio' => '1',
    'femmina' => '2'
  ];

  private $tipoFornitore = 'PFRI';

  private $nome;

  private $cognome;

  private $sesso;

  private $dataNascita;

  private $via;

  private $frazione;

  private $localita;

  private $cap;

  private $provinciaResidenza;

  private $codiceComune;

  private $codiceProvincia;

  private $codiceRegione;

  private $telefono;

  private $codiceFiscale;

  private $provinciaNascita;

  private $comuneNascita;

  private $iban;


  /**
   * @return mixed
   */
  public function getTipoFornitore()
  {
    return $this->tipoFornitore;
  }

  /**
   * @return mixed
   */
  public function getNome()
  {
    return $this->nome;
  }

  /**
   * @param mixed $nome
   */
  public function setNome($nome): void
  {
    $this->nome = $nome;
  }

  /**
   * @return mixed
   */
  public function getCognome()
  {
    return $this->cognome;
  }

  /**
   * @param mixed $cognome
   */
  public function setCognome($cognome): void
  {
    $this->cognome = $cognome;
  }

  /**
   * @return mixed
   */
  public function getDataNascita()
  {
    return $this->dataNascita;
  }

  /**
   * @param mixed $dataNascita
   */
  public function setDataNascita($dataNascita): void
  {
    try {
      $parsedDate = new DateTime($dataNascita);
      $this->dataNascita = $parsedDate->format('Ymd');
    } catch (\Exception $e) {
      $this->dataNascita = null;
    }
  }

  /**
   * @param mixed $dataNascita
   */
  public function setDataNascitaFromFormat($dataNascita, $format = 'd/m/Y'): void
  {
    try {
      $parsedDate = DateTime::createFromFormat($format, $dataNascita);
      $this->dataNascita = $parsedDate->format('Ymd');
    } catch (\Exception $e) {
      $this->dataNascita = null;
    }
  }


  /**
   * @return mixed
   */
  public function getVia()
  {
    return $this->via;
  }

  /**
   * @param mixed $via
   */
  public function setVia($via): void
  {
    $this->via = $via;
  }

  /**
   * @return mixed
   */
  public function getFrazione()
  {
    return $this->frazione;
  }

  /**
   * @param mixed $frazione
   */
  public function setFrazione($frazione): void
  {
    $this->frazione = $frazione;
  }

  /**
   * @return mixed
   */
  public function getLocalita()
  {
    return $this->localita;
  }

  /**
   * @param mixed $localita
   */
  public function setLocalita($localita): void
  {
    $this->localita = $localita;
  }

  /**
   * @return mixed
   */
  public function getCap()
  {
    return $this->cap;
  }

  /**
   * @param mixed $cap
   */
  public function setCap($cap): void
  {
    $this->cap = $cap;
  }

  /**
   * @return mixed
   */
  public function getProvinciaResidenza()
  {
    return $this->provinciaResidenza;
  }

  /**
   * @param mixed $provinciaResidenza
   */
  public function setProvinciaResidenza($provinciaResidenza): void
  {
    $this->provinciaResidenza = $provinciaResidenza;
  }



  /**
   * @return mixed
   */
  public function getCodiceComune()
  {
    return $this->codiceComune;
  }

  /**
   * @param mixed $codiceComune
   */
  public function setCodiceComune($codiceComune): void
  {
    $this->codiceComune = trim($codiceComune);
  }

  /**
   * @return mixed
   */
  public function getCodiceProvincia()
  {
    return $this->codiceProvincia;
  }

  /**
   * @param mixed $codiceProvincia
   */
  public function setCodiceProvincia($codiceProvincia): void
  {
    $this->codiceProvincia = trim($codiceProvincia);
  }

  /**
   * @return mixed
   */
  public function getCodiceRegione()
  {
    return $this->codiceRegione;
  }

  /**
   * @param mixed $codiceRegione
   */
  public function setCodiceRegione($codiceRegione): void
  {
    $this->codiceRegione = trim($codiceRegione);
  }

  /**
   * @return mixed
   */
  public function getTelefono()
  {
    return $this->telefono;
  }

  /**
   * @param mixed $telefono
   */
  public function setTelefono($telefono): void
  {
    $this->telefono = $telefono;
  }

  /**
   * @return mixed
   */
  public function getCodiceFiscale()
  {
    return $this->codiceFiscale;
  }

  /**
   * @param mixed $codiceFiscale
   */
  public function setCodiceFiscale($codiceFiscale): void
  {
    $this->codiceFiscale = $codiceFiscale;
  }

  /**
   * @return mixed
   */
  public function getProvinciaNascita()
  {
    return $this->provinciaNascita;
  }

  /**
   * @param mixed $provinciaNascita
   */
  public function setProvinciaNascita($provinciaNascita): void
  {
    $this->provinciaNascita = trim($provinciaNascita);
  }

  /**
   * @return mixed
   */
  public function getComuneNascita()
  {
    return $this->comuneNascita;
  }

  /**
   * @param mixed $comuneNascita
   */
  public function setComuneNascita($comuneNascita): void
  {
    $this->comuneNascita = trim($comuneNascita);
  }

  /**
   * @return mixed
   */
  public function getSesso()
  {
    return $this->sesso;
  }

  /**
   * @param mixed $sesso
   */
  public function setSesso($sesso): void
  {
    if (isset(self::GENDER[trim($sesso)])) {
      $this->sesso = self::GENDER[trim($sesso)];
    } else {
      $this->sesso = '1';
    }
  }


  /**
   * @return mixed
   */
  public function getIban()
  {
    return $this->iban;
  }

  /**
   * @param mixed $iban
   */
  public function setIban($iban): void
  {
    $this->iban = $iban;
  }


  public function toXml()
  {
    $xml = new SimpleXMLElement('<m:InserimentoFornitoreIn xmlns:m="http://www.types.ice.infotn.it" />');

    // Parametri
    $parametri = $xml->addChild('m:Parametri');
    $parametri->addChild('m:CodiceApplicazione', $this->codiceApplicazione);
    $parametri->addChild('m:Societa', $this->societa);
    $parametri->addChild('m:CodiceFornitore', $this->codiceFornitoreApplicazione);
    $parametri->addChild('m:Utente', $this->utente);

    $fornitoreIn = $xml->addChild('m:FornitoreIn');

    // AnagraficaFornitore
    $anagraficaFornitore = $fornitoreIn->addChild('m:AnagraficaFornitore');

    // TipoFornitore
    $anagraficaFornitore->addChild('m:TipoFornitore', $this->tipoFornitore);

    // Nominativo
    $nominativo = $anagraficaFornitore->addChild('m:Nominativo');
    $nominativo->addChild('m:Nome1', $this->cognome);
    $nominativo->addChild('m:Nome2', $this->nome);

    // Indirizzo
    $indirizzo = $anagraficaFornitore->addChild('m:Indirizzo');
    $indirizzo->addChild('m:Via', $this->via);
    //$indirizzo->addChild('m:Frazione', $this->frazione);
    //$indirizzo->addChild('m:Localita', $this->localita);
    $indirizzo->addChild('m:CAP', $this->cap);
    $indirizzo->addChild('m:ProvinciaResidenza', $this->provinciaResidenza);
    $indirizzo->addChild('m:CodiceComune', $this->codiceComune);
    $indirizzo->addChild('m:CodiceProvincia', $this->codiceProvincia);
    $indirizzo->addChild('m:CodiceRegione', $this->codiceRegione);

    // Comunicazione
    $comunicazione = $anagraficaFornitore->addChild('m:Comunicazione');
    $comunicazione->addChild('m:Telefono', $this->telefono);

    // DatiFiscali
    $datiFiscali = $anagraficaFornitore->addChild('m:DatiFiscali');
    $datiFiscali->addChild('m:CodiceFiscale', $this->codiceFiscale);

    // DatiNascita
    $datiNascita = $anagraficaFornitore->addChild('m:DatiNascita');

    //$datiNascita->addChild('m:CodiceISTAT', $this->provinciaNascita . $this->comuneNascita);
    $datiNascita->addChild('m:DataNascita', $this->dataNascita);
    $datiNascita->addChild('m:Sesso', $this->sesso);

    // MetodiDiPagamento
    $anagraficaFornitore->addChild('m:MetodiDiPagamento', 'B');

    // DatiContoCorrente
    $datiContoCorrente = $fornitoreIn->addChild('m:DatiContoCorrente');

    $datiContoCorrente->addChild('m:ChiavePaese', substr($this->iban, 0, 2));
    $datiContoCorrente->addChild('m:IBAN', $this->iban);
    $datiContoCorrente->addChild('m:ChiaveBanca', substr($this->iban, 5, 10));
    $datiContoCorrente->addChild('m:NumeroConto', substr($this->iban, 15, 12));
    $datiContoCorrente->addChild('m:CIN', substr($this->iban, 4, 1));
    //$datiContoCorrente->addChild('m:ContoCorDef', '1');

    return trim(str_replace(array('<?xml version="1.0"?>'), '', $xml->asXML()));

  }

}
