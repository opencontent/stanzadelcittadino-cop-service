<?php

namespace App\Objects;

use DateTime;
use SimpleXMLElement;

class Impegno extends SapItem
{

  private $testoPosizione;

  private $codiceTransazione = 'FMY1';

  private $progressivo = '1';

  private $identificativoLista;

  private $siglaApplicazione = 'SDC';

  private $data;

  private $testo;

  private $tipoDocumento = 'IP';

  private $categoriaDocumento = '040';

  private $areaFinanziaria = 'PAT0';

  private $dataRegistrazione;

  private $divisaCambio = 'EUR';

  private $flagControlloFornitore = 'N';

  private $importo;

  private $codiceFornitore;

  private $tipoBancaPartner = 1;

  private $dataInizioVersamenti;

  private $annoProvvedimento;

  private $struttura;

  private $numeroProvvedimento;

  private $tipoProvvedimento;

  private $codiceBollo = 'E';

  private $capitolo;

  private $dataScadenza;

  private $centroCosto;

  private $modalitaDiPagamento = 'B';

  private $codicePratica;

  private $chiaveRata;

  private $numeroDocumento;

  private $posizioneDocRiferimento;

  /**
   * Impegno constructor.
   */
  public function __construct()
  {
    $this->dataRegistrazione = date('Ymd');
    $this->dataInizioVersamenti = date('Ymd');
    $this->dataScadenza = date('Y' . '1231');
  }

  /**
   * @return mixed
   */
  public function getTestoPosizione()
  {
    return $this->testoPosizione;
  }

  /**
   * @param mixed $testoPosizione
   */
  public function setTestoPosizione($testoPosizione): void
  {
    $this->testoPosizione = $testoPosizione;
  }

  /**
   * @return string
   */
  public function getCodiceTransazione(): string
  {
    return $this->codiceTransazione;
  }

  /**
   * @param string $codiceTransazione
   */
  public function setCodiceTransazione(string $codiceTransazione): void
  {
    $this->codiceTransazione = $codiceTransazione;
  }

  /**
   * @return string
   */
  public function getProgressivo(): string
  {
    return $this->progressivo;
  }

  /**
   * @param string $progressivo
   */
  public function setProgressivo(string $progressivo): void
  {
    $this->progressivo = $progressivo;
  }

  /**
   * @return mixed
   */
  public function getIdentificativoLista()
  {
    return $this->identificativoLista;
  }

  /**
   * @param mixed $identificativoLista
   */
  public function setIdentificativoLista($identificativoLista): void
  {
    $this->identificativoLista = $identificativoLista;
  }

  /**
   * @return string
   */
  public function getSiglaApplicazione(): string
  {
    return $this->siglaApplicazione;
  }

  /**
   * @param string $siglaApplicazione
   */
  public function setSiglaApplicazione(string $siglaApplicazione): void
  {
    $this->siglaApplicazione = $siglaApplicazione;
  }

  /**
   * @return mixed
   */
  public function getData()
  {
    return $this->data;
  }

  /**
   * @param mixed $data
   */
  public function setData($data): void
  {
    try {
      $parsedDate = new DateTime(data);
      $this->data = $parsedDate->format('Ymd');
    } catch (\Exception $e) {
      $this->data = date('Ymd');
    }
  }

  /**
   * @return mixed
   */
  public function getTesto()
  {
    return $this->testo;
  }

  /**
   * @param mixed $testo
   */
  public function setTesto($testo): void
  {
    $this->testo = $testo;
  }

  /**
   * @return string
   */
  public function getTipoDocumento(): string
  {
    return $this->tipoDocumento;
  }

  /**
   * @param string $tipoDocumento
   */
  public function setTipoDocumento(string $tipoDocumento): void
  {
    $this->tipoDocumento = $tipoDocumento;
  }

  /**
   * @return string
   */
  public function getCategoriaDocumento(): string
  {
    return $this->categoriaDocumento;
  }

  /**
   * @param string $categoriaDocumento
   */
  public function setCategoriaDocumento(string $categoriaDocumento): void
  {
    $this->categoriaDocumento = $categoriaDocumento;
  }

  /**
   * @return string
   */
  public function getAreaFinanziaria(): string
  {
    return $this->areaFinanziaria;
  }

  /**
   * @param string $areaFinanziaria
   */
  public function setAreaFinanziaria(string $areaFinanziaria): void
  {
    $this->areaFinanziaria = $areaFinanziaria;
  }

  /**
   * @return mixed
   */
  public function getDataRegistrazione()
  {
    return $this->dataRegistrazione;
  }

  /**
   * @param mixed $dataRegistrazione
   */
  public function setDataRegistrazione($dataRegistrazione): void
  {
    try {
      $parsedDate = new DateTime($dataRegistrazione);
      $this->dataRegistrazione = $parsedDate->format('Ymd');
    } catch (\Exception $e) {
      $this->data = date('Ymd');
    }
  }

  /**
   * @return string
   */
  public function getDivisaCambio(): string
  {
    return $this->divisaCambio;
  }

  /**
   * @param string $divisaCambio
   */
  public function setDivisaCambio(string $divisaCambio): void
  {
    $this->divisaCambio = $divisaCambio;
  }

  /**
   * @return string
   */
  public function getFlagControlloFornitore(): string
  {
    return $this->flagControlloFornitore;
  }

  /**
   * @param string $flagControlloFornitore
   */
  public function setFlagControlloFornitore(string $flagControlloFornitore): void
  {
    $this->flagControlloFornitore = $flagControlloFornitore;
  }

  /**
   * @return mixed
   */
  public function getImporto()
  {
    return $this->importo;
  }

  /**
   * @param mixed $importo
   */
  public function setImporto($importo): void
  {
    $this->importo = $importo;
  }

  /**
   * @return mixed
   */
  public function getAnnoProvvedimento()
  {
    return $this->annoProvvedimento;
  }

  /**
   * @param mixed $annoProvvedimento
   */
  public function setAnnoProvvedimento($annoProvvedimento): void
  {
    $this->annoProvvedimento = $annoProvvedimento;
  }

  /**
   * @return mixed
   */
  public function getDataInizioVersamenti()
  {
    return $this->dataInizioVersamenti;
  }

  /**
   * @param mixed $dataInizioVersamenti
   */
  public function setDataInizioVersamenti($dataInizioVersamenti): void
  {
    $this->dataInizioVersamenti = $dataInizioVersamenti;
  }

  /**
   * @return mixed
   */
  public function getStruttura()
  {
    return $this->struttura;
  }

  /**
   * @param mixed $struttura
   */
  public function setStruttura($struttura): void
  {
    $this->struttura = $struttura;
  }

  /**
   * @return mixed
   */
  public function getNumeroProvvedimento()
  {
    return $this->numeroProvvedimento;
  }

  /**
   * @param mixed $numeroProvvedimento
   */
  public function setNumeroProvvedimento($numeroProvvedimento): void
  {
    $this->numeroProvvedimento = $numeroProvvedimento;
  }

  /**
   * @return mixed
   */
  public function getTipoProvvedimento()
  {
    return $this->tipoProvvedimento;
  }

  /**
   * @param mixed $tipoProvvedimento
   */
  public function setTipoProvvedimento($tipoProvvedimento): void
  {
    $this->tipoProvvedimento = $tipoProvvedimento;
  }

  public function setProvvedimento($provvedimento)
  {
    $data = explode('-', $provvedimento);
    if (count($data) <= 0) {
      throw new \Exception('Formato del provvedimento non corretto');
    }

    $this->annoProvvedimento = $data[0];
    $this->struttura = $data[1];
    $this->numeroProvvedimento = $data[2];
  }

  /**
   * @return string
   */
  public function getCodiceBollo(): string
  {
    return $this->codiceBollo;
  }

  /**
   * @param string $codiceBollo
   */
  public function setCodiceBollo(string $codiceBollo): void
  {
    $this->codiceBollo = $codiceBollo;
  }

  /**
   * @return mixed
   */
  public function getCodiceFornitore()
  {
    return $this->codiceFornitore;
  }

  /**
   * @param mixed $codiceFornitore
   */
  public function setCodiceFornitore($codiceFornitore): void
  {
    $this->codiceFornitore = str_pad($codiceFornitore, 10, "0", STR_PAD_LEFT);

  }

  /**
   * @return int
   */
  public function getTipoBancaPartner(): int
  {
    return $this->tipoBancaPartner;
  }

  /**
   * @param int $tipoBancaPartner
   */
  public function setTipoBancaPartner(int $tipoBancaPartner): void
  {
    $this->tipoBancaPartner = $tipoBancaPartner;
  }

  /**
   * @return mixed
   */
  public function getCapitolo()
  {
    return $this->capitolo;
  }

  /**
   * @param mixed $capitolo
   */
  public function setCapitolo($capitolo): void
  {
    $this->capitolo = $capitolo;
  }

  /**
   * @return false|string
   */
  public function getDataScadenza()
  {
    return $this->dataScadenza;
  }

  /**
   * @param false|string $dataScadenza
   */
  public function setDataScadenza($dataScadenza): void
  {
    $this->dataScadenza = $dataScadenza;
  }

  /**
   * @return mixed
   */
  public function getCentroCosto()
  {
    return $this->centroCosto;
  }

  /**
   * @param mixed $centroCosto
   */
  public function setCentroCosto($centroCosto): void
  {
    $this->centroCosto = $centroCosto;
  }

  /**
   * @return string
   */
  public function getModalitaDiPagamento(): string
  {
    return $this->modalitaDiPagamento;
  }

  /**
   * @param string $modalitaDiPagamento
   */
  public function setModalitaDiPagamento(string $modalitaDiPagamento): void
  {
    $this->modalitaDiPagamento = $modalitaDiPagamento;
  }

  /**
   * @return mixed
   */
  public function getCodicePratica()
  {
    return $this->codicePratica;
  }

  /**
   * @param mixed $codicePratica
   */
  public function setCodicePratica($codicePratica): void
  {
    $this->codicePratica = $codicePratica;
  }

  /**
   * @return mixed
   */
  public function getChiaveRata()
  {
    return $this->chiaveRata;
  }

  /**
   * @param mixed $chiaveRata
   */
  public function setChiaveRata($chiaveRata): void
  {
    $this->chiaveRata = $chiaveRata;
  }

  /**
   * @return mixed
   */
  public function getNumeroDocumento()
  {
    return $this->numeroDocumento;
  }

  /**
   * @param mixed $numeroDocumento
   */
  public function setNumeroDocumento($numeroDocumento): void
  {
    $this->numeroDocumento = $numeroDocumento;
  }

  /**
   * @return mixed
   */
  public function getPosizioneDocRiferimento()
  {
    return $this->posizioneDocRiferimento;
  }

  /**
   * @param mixed $posizioneDocRiferimento
   */
  public function setPosizioneDocRiferimento($posizioneDocRiferimento): void
  {
    $this->posizioneDocRiferimento = $posizioneDocRiferimento;
  }

  public function toXml()
  {
    $xml = new SimpleXMLElement('<m:CreazioneImpegniIn xmlns:m="http://www.types.ice.infotn.it" />');

    $xml->addChild('m:CodiceApplicazione', $this->codiceApplicazione);
    $xml->addChild('m:Utente', $this->utente);

    // CreazioneImpegniTestata
    $creazioneImpegniTestata = $xml->addChild('m:CreazioneImpegniTestata');
    $creazioneImpegniTestata->addChild('m:CodiceTransazione', $this->codiceTransazione);
    $creazioneImpegniTestata->addChild('m:Progressivo', $this->progressivo);
    $creazioneImpegniTestata->addChild('m:IdentificativoLista', $this->identificativoLista);
    $creazioneImpegniTestata->addChild('m:SiglaApplicazione', $this->siglaApplicazione);

    // DatiTestata
    $datiTestata = $creazioneImpegniTestata->addChild('m:DatiTestata');
    $datiTestata->addChild('m:Data', $this->data);
    $datiTestata->addChild('m:Testo', $this->testo);
    $datiTestata->addChild('m:TipoDocumento', $this->tipoDocumento);
    $datiTestata->addChild('m:CategoriaDocumento', $this->categoriaDocumento);
    $datiTestata->addChild('m:Societa', $this->societa);
    $datiTestata->addChild('m:AreaFinanziaria', $this->areaFinanziaria);
    $datiTestata->addChild('m:DataRegistrazione', $this->dataRegistrazione);
    $datiTestata->addChild('m:DivisaCambio', $this->divisaCambio);

    $creazioneImpegniTestata->addChild('m:DataInizioVersamenti', $this->dataInizioVersamenti);

    // Dati provvedimento
    $datiProvvedimento = $creazioneImpegniTestata->addChild('m:DatiProvvedimento');
    $datiProvvedimento->addChild('m:AnnoProvvedimento', $this->annoProvvedimento); // 2020
    $datiProvvedimento->addChild('m:Struttura', $this->struttura); // S039
    $datiProvvedimento->addChild('m:NumeroProvvedimento', $this->numeroProvvedimento); // 28
    $datiProvvedimento->addChild('m:TipoProvvedimento', $this->tipoProvvedimento); // DTD

    // Dati AltriDati
    $altriDati = $creazioneImpegniTestata->addChild('m:AltriDati');
    $altriDati->addChild('m:CodiceBollo', $this->codiceBollo);

    // CreazioneImpegniPosizione
    $creazioneImpegniPosizione = $xml->addChild('m:CreazioneImpegniPosizione');
    $creazioneImpegniPosizione->addChild('m:CodiceTransazione', $this->codiceTransazione);
    $creazioneImpegniPosizione->addChild('m:Progressivo', $this->progressivo);
    $creazioneImpegniPosizione->addChild('m:FlagControlloFornitore', $this->flagControlloFornitore);
    $creazioneImpegniPosizione->addChild('m:Importo', $this->importo);

    // DatiFornitore
    $datiFornitore = $creazioneImpegniPosizione->addChild('m:DatiFornitore');
    $datiFornitore->addChild('m:CodiceFornitore', $this->codiceFornitore);
    $datiFornitore->addChild('m:TipoBancaPartner', $this->tipoBancaPartner);

    // DatiPosizione
    $datiPosizione = $creazioneImpegniPosizione->addChild('m:DatiPosizione');
    $datiPosizione->addChild('m:Testo', $this->testoPosizione);
    $datiPosizione->addChild('m:Capitolo', $this->capitolo);
    $datiPosizione->addChild('m:DataScadenza', $this->dataScadenza); // 20201231
    $datiPosizione->addChild('m:CentroCosto', $this->centroCosto); // D336
    $datiPosizione->addChild('m:ModalitaDiPagamento', $this->modalitaDiPagamento); // B
    //$datiPosizione->addChild('m:CodicePrelFondoUE', '2016740');
    //$datiPosizione->addChild('m:CodiceVLivello', '1');

    $docRiferimento = $creazioneImpegniPosizione->addChild('m:DocRiferimento');
    $docRiferimento->addChild('m:NumeroDocumento', $this->numeroDocumento);
    $docRiferimento->addChild('m:Posizione', $this->posizioneDocRiferimento);

    // IdentificativoApplicazioneSettore
    $identificativoApplicazioneSettore = $creazioneImpegniPosizione->addChild('m:IdentificativoApplicazioneSettore');
    $identificativoApplicazioneSettore->addChild('m:CodicePratica', $this->codicePratica);
    $identificativoApplicazioneSettore->addChild('m:ChiaveRata', $this->chiaveRata);

    return trim(str_replace(array('<?xml version="1.0"?>'), '', $xml->asXML()));

  }
}
