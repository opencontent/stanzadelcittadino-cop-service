<?php

namespace App\Objects;

use DateTime;
use Illuminate\Support\Collection;
use SimpleXMLElement;

class ImpegnoMulti extends SapItem
{

  private $codiceTransazione = 'FMY1';

  private $progressivo = '1';

  private $identificativoLista;

  private $siglaApplicazione = 'SDC';

  private $data;

  private $testo;

  private $tipoDocumento = 'IP';

  private $categoriaDocumento = '040';

  private $areaFinanziaria = 'PAT0';

  private $dataRegistrazione;

  private $divisaCambio = 'EUR';

  private $dataInizioVersamenti;

  private $annoProvvedimento;

  private $struttura;

  private $numeroProvvedimento;

  private $tipoProvvedimento;

  private $codiceBollo = 'E';

  private $impegniPosizione;


  /**
   * Impegno constructor.
   */
  public function __construct()
  {
    $this->dataRegistrazione = date('Ymd');
    $this->dataInizioVersamenti = date('Ymd');
    $this->impegniPosizione = new Collection();
  }

  /**
   * @return string
   */
  public function getCodiceTransazione(): string
  {
    return $this->codiceTransazione;
  }

  /**
   * @param string $codiceTransazione
   */
  public function setCodiceTransazione(string $codiceTransazione): void
  {
    $this->codiceTransazione = $codiceTransazione;
  }

  /**
   * @return string
   */
  public function getProgressivo(): string
  {
    return $this->progressivo;
  }

  /**
   * @param string $progressivo
   */
  public function setProgressivo(string $progressivo): void
  {
    $this->progressivo = $progressivo;
  }

  /**
   * @return mixed
   */
  public function getIdentificativoLista()
  {
    return $this->identificativoLista;
  }

  /**
   * @param mixed $identificativoLista
   */
  public function setIdentificativoLista($identificativoLista): void
  {
    $this->identificativoLista = $identificativoLista;
  }

  /**
   * @return string
   */
  public function getSiglaApplicazione(): string
  {
    return $this->siglaApplicazione;
  }

  /**
   * @param string $siglaApplicazione
   */
  public function setSiglaApplicazione(string $siglaApplicazione): void
  {
    $this->siglaApplicazione = $siglaApplicazione;
  }

  /**
   * @return mixed
   */
  public function getData()
  {
    return $this->data;
  }

  /**
   * @param mixed $data
   */
  public function setData($data): void
  {
    try {
      $parsedDate = new DateTime($data);
      $this->data = $parsedDate->format('Ymd');
    } catch (\Exception $e) {
      $this->data = date('Ymd');
    }
  }

  /**
   * @return mixed
   */
  public function getTesto()
  {
    return $this->testo;
  }

  /**
   * @param mixed $testo
   */
  public function setTesto($testo): void
  {
    $this->testo = $testo;
  }

  /**
   * @return string
   */
  public function getTipoDocumento(): string
  {
    return $this->tipoDocumento;
  }

  /**
   * @param string $tipoDocumento
   */
  public function setTipoDocumento(string $tipoDocumento): void
  {
    $this->tipoDocumento = $tipoDocumento;
  }

  /**
   * @return string
   */
  public function getCategoriaDocumento(): string
  {
    return $this->categoriaDocumento;
  }

  /**
   * @param string $categoriaDocumento
   */
  public function setCategoriaDocumento(string $categoriaDocumento): void
  {
    $this->categoriaDocumento = $categoriaDocumento;
  }

  /**
   * @return string
   */
  public function getAreaFinanziaria(): string
  {
    return $this->areaFinanziaria;
  }

  /**
   * @param string $areaFinanziaria
   */
  public function setAreaFinanziaria(string $areaFinanziaria): void
  {
    $this->areaFinanziaria = $areaFinanziaria;
  }

  /**
   * @return mixed
   */
  public function getDataRegistrazione()
  {
    return $this->dataRegistrazione;
  }

  /**
   * @param mixed $dataRegistrazione
   */
  public function setDataRegistrazione($dataRegistrazione): void
  {
    try {
      $parsedDate = new DateTime($dataRegistrazione);
      $this->dataRegistrazione = $parsedDate->format('Ymd');
    } catch (\Exception $e) {
      $this->data = date('Ymd');
    }
  }

  /**
   * @return string
   */
  public function getDivisaCambio(): string
  {
    return $this->divisaCambio;
  }

  /**
   * @param string $divisaCambio
   */
  public function setDivisaCambio(string $divisaCambio): void
  {
    $this->divisaCambio = $divisaCambio;
  }

  /**
   * @return mixed
   */
  public function getAnnoProvvedimento()
  {
    return $this->annoProvvedimento;
  }

  /**
   * @param mixed $annoProvvedimento
   */
  public function setAnnoProvvedimento($annoProvvedimento): void
  {
    $this->annoProvvedimento = $annoProvvedimento;
  }

  /**
   * @return mixed
   */
  public function getDataInizioVersamenti()
  {
    return $this->dataInizioVersamenti;
  }

  /**
   * @param mixed $dataInizioVersamenti
   */
  public function setDataInizioVersamenti($dataInizioVersamenti): void
  {
    $this->dataInizioVersamenti = $dataInizioVersamenti;
  }

  /**
   * @return mixed
   */
  public function getStruttura()
  {
    return $this->struttura;
  }

  /**
   * @param mixed $struttura
   */
  public function setStruttura($struttura): void
  {
    $this->struttura = $struttura;
  }

  /**
   * @return mixed
   */
  public function getNumeroProvvedimento()
  {
    return $this->numeroProvvedimento;
  }

  /**
   * @param mixed $numeroProvvedimento
   */
  public function setNumeroProvvedimento($numeroProvvedimento): void
  {
    $this->numeroProvvedimento = $numeroProvvedimento;
  }

  /**
   * @return mixed
   */
  public function getTipoProvvedimento()
  {
    return $this->tipoProvvedimento;
  }

  /**
   * @param mixed $tipoProvvedimento
   */
  public function setTipoProvvedimento($tipoProvvedimento): void
  {
    $this->tipoProvvedimento = $tipoProvvedimento;
  }

  public function setProvvedimento($provvedimento)
  {
    $data = explode('-', $provvedimento);
    if (count($data) <= 0) {
      throw new \Exception('Formato del provvedimento non corretto');
    }

    $this->annoProvvedimento = $data[0];
    $this->struttura = $data[1];
    $this->numeroProvvedimento = $data[2];
  }

  /**
   * @return string
   */
  public function getCodiceBollo(): string
  {
    return $this->codiceBollo;
  }

  /**
   * @param string $codiceBollo
   */
  public function setCodiceBollo(string $codiceBollo): void
  {
    $this->codiceBollo = $codiceBollo;
  }

  /**
   * @return Collection
   */
  public function getImpegniPosizione(): Collection
  {
    return $this->impegniPosizione;
  }

  /**
   * @param Collection $impegniPosizione
   */
  public function setImpegniPosizione(Collection $impegniPosizione): void
  {
    $this->impegniPosizione = $impegniPosizione;
  }

  public function addImpegnoPosizione(PosizioneImpegno $posizioneImpegno) {
    $this->impegniPosizione->push($posizioneImpegno);
  }

  public function removeImpegnoPosizione(PosizioneImpegno $posizioneImpegno) {
    $this->impegniPosizione->forget($posizioneImpegno);
  }


  public function toXml()
  {
    $xml = new SimpleXMLElement('<m:CreazioneImpegniIn xmlns:m="http://www.types.ice.infotn.it" />');

    $xml->addChild('m:CodiceApplicazione', $this->codiceApplicazione);
    $xml->addChild('m:Utente', $this->utente);

    // CreazioneImpegniTestata
    $creazioneImpegniTestata = $xml->addChild('m:CreazioneImpegniTestata');
    $creazioneImpegniTestata->addChild('m:CodiceTransazione', $this->codiceTransazione);
    $creazioneImpegniTestata->addChild('m:Progressivo', $this->progressivo);
    $creazioneImpegniTestata->addChild('m:IdentificativoLista', $this->identificativoLista);
    $creazioneImpegniTestata->addChild('m:SiglaApplicazione', $this->siglaApplicazione);

    // DatiTestata
    $datiTestata = $creazioneImpegniTestata->addChild('m:DatiTestata');
    $datiTestata->addChild('m:Data', $this->data);
    $datiTestata->addChild('m:Testo', $this->testo);
    $datiTestata->addChild('m:TipoDocumento', $this->tipoDocumento);
    $datiTestata->addChild('m:CategoriaDocumento', $this->categoriaDocumento);
    $datiTestata->addChild('m:Societa', $this->societa);
    $datiTestata->addChild('m:AreaFinanziaria', $this->areaFinanziaria);
    $datiTestata->addChild('m:DataRegistrazione', $this->dataRegistrazione);
    $datiTestata->addChild('m:DivisaCambio', $this->divisaCambio);

    $creazioneImpegniTestata->addChild('m:DataInizioVersamenti', $this->dataInizioVersamenti);

    // Dati provvedimento
    $datiProvvedimento = $creazioneImpegniTestata->addChild('m:DatiProvvedimento');
    $datiProvvedimento->addChild('m:AnnoProvvedimento', $this->annoProvvedimento); // 2020
    $datiProvvedimento->addChild('m:Struttura', $this->struttura); // S039
    $datiProvvedimento->addChild('m:NumeroProvvedimento', $this->numeroProvvedimento); // 28
    $datiProvvedimento->addChild('m:TipoProvvedimento', $this->tipoProvvedimento); // DTD

    // Dati AltriDati
    $altriDati = $creazioneImpegniTestata->addChild('m:AltriDati');
    $altriDati->addChild('m:ANFI', 'Y');
    $altriDati->addChild('m:CodiceBollo', $this->codiceBollo);

    // Inserire lista impegni posizione

    $this->impegniPosizione->each(function ($item, $key) use ($xml) {
      /** @var PosizioneImpegno $item  */

      // CreazioneImpegniPosizione
      $creazioneImpegniPosizione = $xml->addChild('m:CreazioneImpegniPosizione');
      $creazioneImpegniPosizione->addChild('m:CodiceTransazione', $this->codiceTransazione);
      $creazioneImpegniPosizione->addChild('m:Progressivo', $item->getProgressivo());
      $creazioneImpegniPosizione->addChild('m:FlagControlloFornitore', $item->getFlagControlloFornitore());
      $creazioneImpegniPosizione->addChild('m:Importo', $item->getImporto());

      // DatiFornitore
      $datiFornitore = $creazioneImpegniPosizione->addChild('m:DatiFornitore');
      $datiFornitore->addChild('m:CodiceFornitore', $item->getCodiceFornitore());
      $datiFornitore->addChild('m:TipoBancaPartner', $item->getTipoBancaPartner());

      // DatiPosizione
      $datiPosizione = $creazioneImpegniPosizione->addChild('m:DatiPosizione');
      $datiPosizione->addChild('m:Testo', $item->getTesto());
      $datiPosizione->addChild('m:Capitolo', $item->getCapitolo());
      $datiPosizione->addChild('m:DataScadenza', $item->getDataScadenza()); // 20201231
      $datiPosizione->addChild('m:CentroCosto', $item->getCentroCosto()); // D336
      $datiPosizione->addChild('m:ModalitaDiPagamento', $item->getModalitaDiPagamento()); // B
      //$datiPosizione->addChild('m:CodicePrelFondoUE', '2016740');
      //$datiPosizione->addChild('m:CodiceVLivello', '1');

      $docRiferimento = $creazioneImpegniPosizione->addChild('m:DocRiferimento');
      $docRiferimento->addChild('m:NumeroDocumento', $item->getNumeroDocumento());
      $docRiferimento->addChild('m:Posizione', $item->getDocRiferimento());

      // IdentificativoApplicazioneSettore
      $identificativoApplicazioneSettore = $creazioneImpegniPosizione->addChild('m:IdentificativoApplicazioneSettore');
      $identificativoApplicazioneSettore->addChild('m:CodicePratica', $this->numeroProvvedimento .  '-' .  $item->getCodicePratica());
      //$identificativoApplicazioneSettore->addChild('m:CodicePratica', $item->getCodicePratica());
      $identificativoApplicazioneSettore->addChild('m:ChiaveRata', $item->getChiaveRata());

      // Anfi
      $anfi = $creazioneImpegniPosizione->addChild('m:DatiANFI');
      $anfi->addChild('m:AttEconDiv', '99');
      $anfi->addChild('m:AttEconGru', '9');
      $anfi->addChild('m:AttEconCla', '9');
      $anfi->addChild('m:AttEconCat', '9');
      $anfi->addChild('m:AttEconSottoCat', '9');
      $anfi->addChild('m:AttEconAnno', '2007');
      $anfi->addChild('m:Localita', strtoupper($item->getLocalitaFornitore()));
      $anfi->addChild('m:FlagAtt', 'Y');

    });

    return trim(str_replace(array('<?xml version="1.0"?>'), '', $xml->asXML()));

  }
}
