<?php


/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

use App\Mapper\BonusBeb;
use App\Mapper\BonusVacanza;
use App\Models\Application;
use App\Objects\Anagrafica;
use App\Objects\Impegno;
use App\Objects\ImpegnoMulti;
use App\Utilities\CopSoap;
use App\Utilities\Csv;
use App\Utilities\Db;
use App\Utilities\Municipality;
use App\Utilities\PseudoCrypt;
use Illuminate\Support\Facades\Log;
use Ramsey\Uuid\Uuid;

$router->get(
  '/',
  function () use ($router) {
    return $router->app->version();
  }
);

$router->get(
  'suppliers',
  function () use ($router) {

    $file = '../data/applications.csv';
    $csv = Csv::csvToArray($file, ',');

    $results = [];
    $stats = [
      'count' => 0,
      'not_processable' => 0,
      'already_processed' => 0,
      'error' => 0,
      'success' => 0
    ];

    foreach ($csv as $k => $row) {

      $stats['count'] += 1;
      $application = Application::where('remote_id', $row['id'])->first();

      if ( $application == null) {

        $application = new Application();
        $application->setAttribute('remote_id', $row['id']);
        $application->save();

      } else {

        // Controllo se la pratica è stata già processata con succeso (db)
        if ( !empty($application->getAttribute('supplier_code'))) {
          $msg = 'La pratica con id: ' . $row['id'] . 'è già stata processata';
          Log::info($msg);
          $results[$row['id']] = $msg;
          $stats['already_processed'] += 1;
          continue;
        }
      }

      // Controllo se la pratica deve essere processata
      if (strtolower($row['Stato verifica']) != 'ok ammissibile' || empty($row['PROVVEDIMENTO'])) {

        $msg = 'La pratica con id: ' . $row['id'] . ' non è processabile';
        $application->setAttribute('status', 'error');
        $application->setAttribute('logs', $msg);
        $application->save();

        Log::info($msg);
        $results[$row['id']] = $msg;
        $stats['not_processable'] += 1;
        continue;
      }

      // Controllo se la pratica ha già un codice fornitore
      if (!empty($row['CREAZIONE_FORNITORE'])) {

        $msg = 'La pratica con id: ' . $row['id'] . ' ha già un codice fornitore: ' . $row['CREAZIONE_FORNITORE'];

        $application->setAttribute('status', 'success');
        $application->setAttribute('supplier_code', $row['CREAZIONE_FORNITORE']);
        $application->setAttribute('logs', $msg);
        $application->save();

        Log::info($msg);
        $results[$row['id']] = $msg;
        $stats['already_processed'] += 1;

        continue;
      }

      $anagrafica = null;
      $codiceFornitore = $row['CREAZIONE_FORNITORE'];

      if (empty(trim($codiceFornitore))) {
        try {
          // Invio Anagrafica a ws
          $anagrafica = BonusVacanza::mapAnagrafica($row);
          //$anagrafica = BonusBeb::mapAnagrafica($row);
          $codiceFornitore = CopSoap::createAnagrafica($anagrafica->toXml());
        } catch (\Exception $e) {
          $application->setAttribute('status', 'error');
          $application->setAttribute('logs', $e->getMessage());
          $application->save();

          $stats['error'] += 1;
          $results[$row['id']] = $e->getMessage();
          continue;
        }
      }

      $rowSuccess = array(
        'codice_fornitore' => $codiceFornitore
      );

      $application->setAttribute('status', 'success');
      $application->setAttribute('supplier_code', $codiceFornitore);
      $application->save();

      $stats['success'] += 1;
      $results[$row['id']] = $rowSuccess;
    }
    dump($stats);
    dd($results);
  }
);

$router->get(
  'founds',
  function () use ($router) {

    $env = 'production';

    $file = '../data/applications-465.csv';
    $csv = Csv::csvToArray($file, ',');

    $data = [];
    $results = [];
    $stats = [
      'count' => 0,
      'not_processable' => 0,
      'already_processed' => 0,
      'error' => 0,
      'success' => 0
    ];
    foreach ($csv as $k => $row) {

      $stats['count'] += 1;
      $application = Application::where('remote_id', $row['id'])->first();

      if ($application == null) {
        $application = new Application();
        $application->setAttribute('remote_id', $row['id']);
        $application->save();

      }

      // Controllo se la pratica ha già un codice impegno sul db
      if (!empty($application->getAttribute('found_code'))) {
        $msg = 'La pratica con id: '.$row['id'].'è già stata processata';
        Log::info($msg);
        $results[$row['id']] = $msg;
        $stats['already_processed'] += 1;
        continue;
      }

      // Controllo se la pratica deve essere processata
      if (strtolower($row['Stato verifica']) != 'ok ammissibile' || empty($row['PROVVEDIMENTO'])) {
        $msg = 'La pratica con id: '.$row['id'].' non è processabile';
        Db::saveApplication($application, [
          'status' => 'error',
          'logs' => $msg
        ]);

        Log::info($msg);
        $results[$row['id']] = $msg;
        $stats['not_processable'] += 1;
        continue;
      }

      // Controllo se la pratica è stata già processata (csv)
      if (!empty(trim($row['CREAZIONE_FORNITORE'])) && !empty(trim($row['CREAZIONE_IMPEGNO']))) {
        $msg = 'La pratica con id: '.$row['id'].' ha già un codice fornitore ed un codice impegno';
        Db::saveApplication($application, [
          'status' => 'error',
          'logs' => $msg
        ]);

        Log::info($msg);
        $results[$row['id']] = $msg;
        $stats['already_processed'] += 1;
        continue;
      }

      $anagrafica = null;

      // Codice Fornitore
      if (isset($row['FILI']) && !empty(trim($row['FILI']))) {
        $codiceFornitore = $row['FILI'];
      } else {
        $codiceFornitore = $row['CREAZIONE_FORNITORE'];
      }

      if (empty(trim($codiceFornitore))) {
        $msg = 'Codice forinitore non presente';
        Db::saveApplication($application, [
          'status' => 'error',
          'logs' => $msg
        ]);
        dump($application->getAttribute('remote_id'));
        $stats['error'] += 1;
        $results[$row['id']] = $msg;
        continue;
      }

      $row['codice_fornitore'] = $codiceFornitore;
      $row['codice_pratica'] = $application->getAttribute('id');

      if (!isset($data[$row['PROVVEDIMENTO']])) {
        //$data[$row['PROVVEDIMENTO']] = BonusBeb::mapImpegnoMulti($row);
        $data[$row['PROVVEDIMENTO']] = BonusVacanza::mapImpegnoMulti($row);
      }
      $impegno = $data[$row['PROVVEDIMENTO']];
      //$posizioneImpegno = BonusBeb::mapPosizioneImpegno($row);
      $posizioneImpegno = BonusVacanza::mapPosizioneImpegno($row);
      $impegno->addImpegnoPosizione($posizioneImpegno);
    }

    foreach ($data as $code => $d) {
      try {
        //dd($d->toXml());
        $codiceImpegno = CopSoap::createImpegno($d->toXml());
        if (!empty($codiceImpegno)) {
          dump($codiceImpegno);
          /*foreach ($codiceImpegno as $k => $v) {
            $application = Application::where('id', $k)->first();
            if ($application instanceof Application) {
              Db::saveApplication($application, [
                'found_code' => $v['numero_documento']
              ]);
            }
          }*/
        }
      } catch (\Exception $e) {
        dump($e);
        dump(json_decode($e->getMessage(), true));
        //Log::error("C'è stato un errore nella creazione dell'impegno con provvedimento: " . $code, json_decode($e->getMessage(), true));
      }
    }

    dump($stats);
    //dd($results);
  }
);
